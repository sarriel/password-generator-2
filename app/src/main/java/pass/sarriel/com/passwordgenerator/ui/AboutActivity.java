package pass.sarriel.com.passwordgenerator.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pass.sarriel.com.passwordgenerator.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}

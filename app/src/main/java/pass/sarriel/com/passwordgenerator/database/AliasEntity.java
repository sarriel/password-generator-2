package pass.sarriel.com.passwordgenerator.database;

import io.realm.RealmObject;

/**
 * Created by matej on 18.3.2017.
 */

public class AliasEntity extends RealmObject {

    private String alias;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}

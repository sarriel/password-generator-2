package pass.sarriel.com.passwordgenerator.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import pass.sarriel.com.passwordgenerator.R;
import pass.sarriel.com.passwordgenerator.ui.AboutActivity;
import pass.sarriel.com.passwordgenerator.ui.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initializeFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void initializeFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_content, MainGeneratorFragment.getInstance());
        transaction.addToBackStack(MainGeneratorFragment.class.getSimpleName());
        transaction.commit();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_generator) {
            loadFragment(MainGeneratorFragment.class);
        } else if (id == R.id.nav_aliases) {
            loadFragment(MainAliasesFragment.class);
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(getBaseContext(), SettingsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(getBaseContext(), AboutActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(Class fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_content, MainGeneratorFragment.getInstance(), MainGeneratorFragment.class.getCanonicalName());
        transaction.commit();

        // do not load same fragment that we have already
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_content);
        if (currentFragment != null && currentFragment.getClass().equals(fragment)) {
            return;
        }

        try {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
            ft.replace(R.id.main_content, (Fragment) fragment.newInstance());
            ft.addToBackStack(fragment.getCanonicalName());
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

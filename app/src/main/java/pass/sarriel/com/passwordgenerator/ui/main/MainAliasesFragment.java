package pass.sarriel.com.passwordgenerator.ui.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.Sort;
import pass.sarriel.com.passwordgenerator.R;
import pass.sarriel.com.passwordgenerator.database.AliasEntity;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainAliasesFragment extends Fragment {

    private Realm realm;
    private EditText aliasInput;

    public MainAliasesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.realm = Realm.getDefaultInstance();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.main_content_aliases, container, false);
        //setup recycler
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.main_content_aliases_recycler);
        rv.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(new AliasAdapter(this.realm.where(AliasEntity.class).findAllSorted("alias", Sort.ASCENDING),true));
        //addition of new aliases
        this.aliasInput = (EditText) view.findViewById(R.id.main_content_aliases_newalias_text);
        this.aliasInput.setOnEditorActionListener(getAliasEditorActionListener());
        view.findViewById(R.id.main_content_aliases_newalias_submit).setOnClickListener(getSubmitAliasListener());
        return view;
    }

    private TextView.OnEditorActionListener getAliasEditorActionListener() {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                addNewAlias();
                return true;
            }
        };
    }

    private View.OnClickListener getSubmitAliasListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewAlias();
            }
        };
    }

    private void addNewAlias() {
        String alias = aliasInput.getText().toString();

        if (alias.isEmpty()) {
            Snackbar.make(getView(), "Alias cannot be empty", BaseTransientBottomBar.LENGTH_LONG).show();
            return;
        }

        if (this.realm.where(AliasEntity.class).equalTo("alias", alias).findFirst() != null) {
            Snackbar.make(getView(), "Alias already exists", BaseTransientBottomBar.LENGTH_LONG).show();
            return;
        }

        AliasEntity aliasEntity = new AliasEntity();
        aliasEntity.setAlias(alias);
        aliasInput.setText(null);

        realm.beginTransaction();
        realm.insert(aliasEntity);
        realm.commitTransaction();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.realm.close();
        this.realm = null;
    }

    private class AliasAdapter extends RealmRecyclerViewAdapter<AliasEntity, AliasVH> {

        public AliasAdapter(@Nullable OrderedRealmCollection<AliasEntity> data, boolean autoUpdate) {
            super(data, autoUpdate);
        }

        @Override
        public AliasVH onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_content_aliases_item, parent, false);
            return new AliasVH(view);
        }

        @Override
        public void onBindViewHolder(AliasVH holder, int position) {
            holder.set(getData().get(position));
        }
    }

    private class AliasVH extends RecyclerView.ViewHolder {
        private TextView textAlias;
        private AliasEntity alias;

        public AliasVH(View itemView) {
            super(itemView);
            this.textAlias = (TextView) itemView.findViewById(R.id.main_content_aliases_item_alias);
            itemView.findViewById(R.id.main_content_aliases_item_delete).setOnClickListener(getDeleteListener());
        }

        public void set(AliasEntity alias) {
            this.alias = alias;
            this.textAlias.setText(alias.getAlias());
        }

        private View.OnClickListener getDeleteListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    realm.beginTransaction();
                    alias.deleteFromRealm();
                    realm.commitTransaction();
                }
            };
        }
    }
}

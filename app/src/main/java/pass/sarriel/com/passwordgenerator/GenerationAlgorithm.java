package pass.sarriel.com.passwordgenerator;

import android.util.Base64;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GenerationAlgorithm {

    private GenerationAlgorithm() {}

    public static String generatePassword(String alias, String secret) {
        String seed = alias.concat(secret);
        try {
            return (Base64.encodeToString(getHash(seed).getBytes(), 0)).substring(17,49);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String getHash(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            return bin2hex(digest.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "x", new BigInteger(1, data));
    }

}
package pass.sarriel.com.passwordgenerator.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pass.sarriel.com.passwordgenerator.R;

public class MainGeneratorFragment extends Fragment {

    public MainGeneratorFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance() {
        return new MainGeneratorFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.main_content_generator, container, false);
    }
}

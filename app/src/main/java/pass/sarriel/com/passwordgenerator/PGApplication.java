package pass.sarriel.com.passwordgenerator;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by matej on 18.3.2017.
 */

public class PGApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getBaseContext());
    }
}
